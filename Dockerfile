FROM ubuntu:16.04

MAINTAINER David Dang <david.dang@manadr.com>
RUN	apt-get update && \
	apt-get install -y --allow-unauthenticated \
	curl \
	git \
	wget \
	python3 \
	python3-dev \
	python3-setuptools \
	libmysqlclient-dev \
	build-essential \
	libssl-dev \
	libffi-dev \
	ruby \
	mysql-client \
	default-jre-headless
RUN easy_install3 pip
RUN pip install --upgrade pip
RUN ln -s /usr/bin/python3 /usr/bin/python
RUN gem install tiller
COPY tiller/ /etc/tiller
COPY apps.conf.example /etc/tiller/templates/apps.erb
RUN mkdir /var/local/facebookchat /var/log/facebookchat
WORKDIR /usr/local/facebookchat
ENV PYTHONUNBUFFERED 1
ENV PYTHONPATH $PYTHONPATH:/usr/local/facebookchat
ADD server/ ./
RUN pip install -r /usr/local/facebookchat/requirements.txt
ADD entrypoint.sh /tmp/entrypoint.sh
RUN chmod a+x /tmp/entrypoint.sh
CMD ["/usr/local/bin/tiller" , "-v"]
HEALTHCHECK --interval=10s --timeout=5s \
      CMD curl -f http://localhost:8000/health/ -H 'accept: application/json'  || exit 1
EXPOSE 8000
