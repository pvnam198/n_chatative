import React, { Component } from 'react';
import { DropdownItem, DropdownMenu, DropdownToggle, Nav } from 'reactstrap';
import PropTypes from 'prop-types';

import { AppHeaderDropdown, AppNavbarBrand, AppSidebarToggler } from '@coreui/react';
import { connect } from 'react-redux';
import Gravatar from 'react-gravatar'
import logo from '../../assets/img/brand/logo.png'
import sygnet from '../../assets/img/brand/favicon.png'
import { URL_ROOT, URL_LOGOUT_REDIRECT } from './../../constants';

const propTypes = {
  children: PropTypes.node,
};

const defaultProps = {};

class DefaultHeader extends Component {
  render() {
    const domain = window.fitiShop.domain;
    const { email, name } = window.fitiShop;
    return (
      <React.Fragment>
        <AppSidebarToggler className="d-lg-none" display="md" mobile />
        <AppNavbarBrand
          full={{ src: logo, width: 115, height: 25, alt: 'My Logo' }}
          minimized={{ src: sygnet, width: 30, height: 30, alt: 'My Logo' }}
        />
        <AppSidebarToggler className="d-md-down-none" display="lg" />

        <Nav className="ml-auto" navbar>
          <AppHeaderDropdown direction="down" style={{ paddingRight: 10 }}>
            <DropdownToggle nav>
              <Gravatar email={email} className="img-avatar" alt="" style={{ width: 35 }} />
              <span className="shop-name">{name}</span>
            </DropdownToggle>
            <DropdownMenu right style={{ right: 'auto' }}>
              <DropdownItem><a target="_blank" href={`http://${domain}`} ><i className="fa fa-shopping-basket"></i> Open your site</a></DropdownItem>
              <DropdownItem><a target="_blank" href={`http://${domain}/admin`}><i className="icon-social-spotify icons"></i> Back to Shopify</a></DropdownItem>
              <DropdownItem><a href={`${URL_ROOT}/logout/?next=${URL_LOGOUT_REDIRECT}`}><i className="fa fa-sign-out"></i> Logout</a></DropdownItem>
            </DropdownMenu>
          </AppHeaderDropdown>
        </Nav>
      </React.Fragment>
    );
  }
}

DefaultHeader.propTypes = propTypes;
DefaultHeader.defaultProps = defaultProps;

const mapStateToProps = state => ({
  user: state.user,
});

export default connect(mapStateToProps)(DefaultHeader);
