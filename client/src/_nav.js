import { URL_HELP } from "./constants";

export default {
  items: [
    {
      title: true,
      name: 'General',
      wrapper: {            // optional wrapper object
        element: '',        // required valid HTML5 element tag
        attributes: {}        // optional valid JS object with JS API naming ex: { className: "my-class", style: { fontFamily: "Verdana" }, id: "my-id"}
      },
      class: ''             // optional class names space delimited list for title item ex: "text-center"
    },
    {
      name: 'Setting',
      url: '/',
      icon: 'icon-settings',
    },
    {
      name: 'Help',
      url: URL_HELP,
      icon: 'fa fa-question',
    }
  ],
};
