import React, { Component } from 'react';
import { Row, Col } from 'reactstrap'
import { connect } from 'react-redux';

import FacebookSettings from './components/FacebookSettings';
import Design from './components/Design';
import MessageCustom from './components/MessageCustom';
import Preview from './components/Preview/PreviewFace';
import * as Actions from '../../redux/actions';
import Loading from './Loading';

class SettingsView extends Component {

  componentDidMount() {
    this.props.getSettings();
  }

  render() {
    const { settings } = this.props;
    const { isFetching } = settings;
    if (isFetching) {
      return (
        <div className="animated fadeIn" style={{ paddingTop: '1rem', height: '100%'  }}>
          <Loading />
        </div>
      )
    }
    return (
      <div className="animated fadeIn" style={{ paddingTop: '1rem'}}>
        <Row>
          <Col xs="12" sm="6">
            <Row>
              <Col>
                <FacebookSettings />
              </Col>
            </Row>
            <Row>
              <Col>
                <Design />
              </Col>
            </Row>
            <Row>
              <Col>
                <MessageCustom />
              </Col>
            </Row>
          </Col>
          <Col xs="12" sm="6">
            <Row>
              <Col>
                <Preview />
              </Col>
            </Row>

          </Col>
        </Row>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  settings: state.settings,
});

const mapDispatchToProps = dispatch => ({
  getSettings: () => dispatch(Actions.getSettings()),
});


export default connect(mapStateToProps, mapDispatchToProps)(SettingsView);
