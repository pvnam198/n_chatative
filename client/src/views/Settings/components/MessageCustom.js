import React, { Component } from 'react';
import { Card, CardHeader, CardBody, CardFooter, FormGroup, Label, Input, Button, Modal, ModalBody, ModalFooter } from 'reactstrap'
import { connect } from 'react-redux';

import * as Actions from './../../../redux/actions';
import * as ActionTypes from "../../../constants/action-types";
import languages from './../../../constants/language-codes';

import PreviewPosition from './PreviewPosition/PreviewPosition';

class MessageCustom extends Component {
  state = {
    isError: false,
    contentModal: '',
    isUpdating: false,
    isOpenPosition: false,
  };
  onChangeGreetingLoggedIn = (e) => {
    this.props.onChangeGreetingLoggedIn(e.currentTarget.value)
  };
  onChangeGreetingLoggedOut = (e) => {
    this.props.onChangeGreetingLoggedOut(e.currentTarget.value)
  };

  onChangePosition = (e) => {
    this.props.onChangePosition(+e.currentTarget.value);
  };
  onChangeShowMobile = (e) => {
    this.props.onChangeShowMobile(+e.currentTarget.checked === 1);
  };
  onChangeShowDesktop = (e) => {
    this.props.onChangeShowDesktop(+e.currentTarget.checked === 1);
  };
  onChangeLanguage = (e) => {
    this.props.onChangeLanguage(e.currentTarget.value);
  };

  toggleOpenPreviewPosition = () => {
    this.setState({ isOpenPosition: !this.state.isOpenPosition});
  }; // touchend

  componentDidMount() {
    const widgetPosition = document.getElementById('widget_position');
    widgetPosition.addEventListener('mouseenter', this.toggleOpenPreviewPosition);
    widgetPosition.addEventListener('touchstart', this.toggleOpenPreviewPosition);
    widgetPosition.addEventListener('mouseleave', this.toggleOpenPreviewPosition);
    widgetPosition.addEventListener('touchend', this.toggleOpenPreviewPosition);
  }

  componentWillUnmount() {
    const widgetPosition = document.getElementById('widget_position');
    widgetPosition.removeEventListener('mouseenter', this.toggleOpenPreviewPosition);
    widgetPosition.removeEventListener('touchstart', this.toggleOpenPreviewPosition);
    widgetPosition.removeEventListener('mouseleave', this.toggleOpenPreviewPosition);
    widgetPosition.removeEventListener('touchend', this.toggleOpenPreviewPosition);
  }

  onSaveSettings = () => {
    this.setState({ isUpdating: true });
    const { settings, updateSettings } = this.props;
    const {
      messageLoggedIn,
      messageLoggedOut,
      isDefault,
      iconButton,
      textButton,
      buttonColor,
      iconTextColor,
      bubbleColor,
      position,
      pageUrl,
      showMobile,
      showDesktop,
      language,
    } = settings.data;

    updateSettings({
      messageLoggedIn,
      messageLoggedOut,
      isDefault,
      iconButton,
      textButton,
      buttonColor,
      iconTextColor,
      bubbleColor,
      position,
      pageUrl,
      showMobile,
      showDesktop,
      language,
    })
      .then(response => {
        console.log(response);
        const { type } = response;
        if (type === ActionTypes.SETTINGS_UPDATE_SUCCESS) {
          this.toggleModal('Success! Facebook Live Chat is active on your store');
        } else {
          this.toggleModal('There something error when update config Facebook Live Chat')
        }
        this.setState({ isUpdating: false });
      });
  };

  onReset = () => {
    this.props.resetDefaultSettings();
  };
  toggleModal = (contentModal = '') => {
    this.setState({
      isError: !this.state.isError,
      contentModal,
    })
  };
  renderLanguage = () => {

  };
  render() {
    const {
      settings,
      onFocusLoggedIn,
      onFocusLoggedOut,
      onBlurLoggedIn,
      onBlurLoggedOut,
    } = this.props;
    const {
      messageLoggedIn,
      messageLoggedOut,
      position,
      showMobile,
      showDesktop,
      language,
    } = settings.data;
    const { isError, contentModal, isUpdating, isOpenPosition } = this.state;
    return (
      <Card>
        <CardHeader>
          Custom Message
        </CardHeader>
        <CardBody>
          <FormGroup>
            <Label htmlFor="text_welcome"><strong>Welcome text (logged in users)</strong></Label>
            <Input
              id="text_welcome"
              value={messageLoggedIn}
              onChange={this.onChangeGreetingLoggedIn}
              onBlur={onBlurLoggedIn}
              onFocus={onFocusLoggedIn}
            />
          </FormGroup>
          <FormGroup>
            <Label htmlFor="text_welcome_guest"><strong>Welcome text (guests)</strong></Label>
            <Input
              id="text_welcome_guest"
              value={messageLoggedOut}
              onChange={this.onChangeGreetingLoggedOut}
              onBlur={onBlurLoggedOut}
              onFocus={onFocusLoggedOut}
            />
          </FormGroup>
          <FormGroup>
            <Label htmlFor="widget_position"><strong>Widget position</strong></Label>
            <input min={0} max={100} step={1} type="range" className="form-control-range" id="widget_position" onChange={this.onChangePosition} value={position} />
            <div style={{ display: 'flex', justifyContent: 'space-between', opacity: 0.7 }}>
              <span>Left screen</span>
              <span>Right screen</span>
            </div>
            <PreviewPosition position={position} target="widget_position" isOpen={isOpenPosition} />
          </FormGroup>
          <FormGroup>
            <Label htmlFor="languages"><strong>Language</strong></Label>
            <Input type="select" name="languages" id="languages" onChange={this.onChangeLanguage} value={language}>
              {
                Object.keys(languages).map(key => {
                  return <option key={languages[key]} value={languages[key]}>{key}</option>
                })
              };

            </Input>
          </FormGroup>
          <FormGroup>
            <div><strong>Show on</strong></div>
            <div>
              <FormGroup check inline style={{ marginRight: '2rem' }}>
                <Input
                  className="form-check-input"
                  type="checkbox"
                  id="show-desktop"
                  name="show-desktop"
                  value="show-desktop"
                  onChange={this.onChangeShowDesktop}
                  checked={showDesktop}
                />
                <Label className="form-check-label" check htmlFor="show-desktop">Desktop</Label>
              </FormGroup>
              <FormGroup check inline>
                <Input
                  className="form-check-input"
                  type="checkbox"
                  id="show-mobile"
                  name="show-mobile"
                  value="show-mobile"
                  onChange={this.onChangeShowMobile}
                  checked={showMobile}
                />
                <Label className="form-check-label" check htmlFor="show-mobile">Mobile</Label>
              </FormGroup>
            </div>
          </FormGroup>
        </CardBody>
        <CardFooter>
          <Button
            style={{ marginRight: 10 }}
            onClick={this.onReset}
          >Reset to default</Button>
          <Button
            onClick={this.onSaveSettings}
            color="primary"
          >
            {isUpdating && <i className="fa fa-spinner fa-lg fa-spin" />}
            {' '}Save & Active
          </Button>
        </CardFooter>
        {isError && <Modal isOpen >
          <ModalBody>
            <span>{contentModal}</span>
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={this.toggleModal}>Ok</Button>
          </ModalFooter>
        </Modal>
        }
      </Card>
    );
  }
}

MessageCustom.propTypes = {

};
MessageCustom.defaultProps = {

};
const mapStateToProps = state => ({
  settings: state.settings,
});

const mapDispatchToProps = dispatch => ({
  onChangeGreetingLoggedIn: value => dispatch(Actions.greetingLoggedInChange(value)),
  onChangeGreetingLoggedOut: value => dispatch(Actions.greetingLoggedOutChange(value)),
  onFocusLoggedIn: () => dispatch(Actions.greetingLoggedInFocus()),
  onFocusLoggedOut: () => dispatch(Actions.greetingLoggedOutFocus()),
  onBlurLoggedIn: () => dispatch(Actions.greetingLoggedInBlur()),
  onBlurLoggedOut: () => dispatch(Actions.greetingLoggedOutBlur()),
  updateSettings: (body) => dispatch(Actions.updateSettings(body)),
  onChangePosition: value => dispatch(Actions.changePosition(value)),
  resetDefaultSettings: () => dispatch(Actions.resetDefault()),
  onChangeShowMobile: value => dispatch(Actions.changeShowMobile(value)),
  onChangeShowDesktop: value => dispatch(Actions.changeShowDesktop(value)),
  onChangeLanguage: value => dispatch(Actions.changeLanguage(value)),
});
export default connect(mapStateToProps, mapDispatchToProps)(MessageCustom);
