/* eslint-disable jsx-a11y/alt-text */
import React, { Component } from 'react';
import './button-icon.css';
import classnames from 'classnames';
import * as Constants from './../../../../constants';


class ButtonIcon extends Component {
  onClick = () => {
    this.props.onClick(this.props.icon);
  };
  render() {
    const { icon, active } = this.props;
    const className = classnames('button-icon', { active: icon === active });
    return (
      <div
        className={className}
        onClick={this.onClick}
      >
        <span
          className={icon}
        >
          {/*{renderIcon(icon)}*/}
        </span>
      </div>
    );
  }
}

ButtonIcon.propTypes = {

};
ButtonIcon.defaultProps = {
  icon: Constants.ICON_1,
  onClick: () => {},
};

export default ButtonIcon;
