import React, { Component } from 'react';
import { Card, CardHeader, CardBody, FormGroup, Label, Input, Button } from 'reactstrap';
import { connect } from 'react-redux';
import * as Actions from "../../../redux/actions";

class FacebookSettings extends Component {
  onChange = (e) => {
    const value = e.currentTarget.value;

    this.props.onChangePageUrl(value);
  };

  render() {
    const { pageUrl } = this.props.settings;
    const pageUrlSetting = pageUrl.endsWith('/') ? pageUrl.slice(0, -1) : pageUrl;
    return (
      <Card>
        <CardHeader>
          <strong>Settings</strong>
        </CardHeader>
        <CardBody>
          <FormGroup>
            <Label htmlFor="ccnumber">Your Facebook page</Label>
            <Input
              type="text"
              id="ccnumber"
              required
              value={pageUrl}
              onChange={this.onChange}
              placeholder="https://www.facebook.com/YourFacebookFanPage"
            />
          </FormGroup>
          <Label>Whitelist your domain to display Message on your website.</Label>
          <Label>For security reasons, the plugin will only render when loaded on a domain that you have whitelisted.</Label>
          <a
            target="_blank"
            href={`${pageUrlSetting}/settings/?tab=messenger_platform`}
            style={pageUrl === '' ? {pointerEvents: 'none'} : {}}
          >
            <Button>
              Go to Facebook settings
            </Button>
          </a>
        </CardBody>
      </Card>
    );
  }
}

FacebookSettings.propTypes = {

};
FacebookSettings.defaultProps = {

};

const mapStateToProps = state => ({
  settings: state.settings.data,
});

const mapDispatchToProps = dispatch => ({
  onChangePageUrl: value => dispatch(Actions.pageUrlChange(value)),
});
export default connect(mapStateToProps, mapDispatchToProps)(FacebookSettings);
