import React, { Component } from 'react';
import { Input, Popover, PopoverBody, InputGroup, InputGroupAddon, Button } from 'reactstrap';
import ReactColor from 'react-color';

class Module extends Component {
  state = {
    isOpen: false,
    value: '',
  };
  toggleOpenColor = () => {
    this.setState({ isOpen: !this.state.isOpen })
  };
  onChange = (color) => {
    // this.setState({ value: color.hex });
    this.props.onChange && this.props.onChange(color.hex);
    console.log(color.hex);
  };
  onChangeInput = (value) => {
    this.setState({ value });
  };
  render() {
    const { id, value } = this.props;
    const { isOpen } = this.state;
    return (
      <span>
        <InputGroup>
          <Input  id={id} value={value} onChange={this.onChangeInput}/>
          <InputGroupAddon addonType="append">
            <Button onClick={this.toggleOpenColor} style={{ backgroundColor: value }}><span>&nbsp;&nbsp;&nbsp;&nbsp;</span></Button>
          </InputGroupAddon>
        </InputGroup>
        <Popover
          isOpen={isOpen}
          toggle={this.toggleOpenColor}
          target={id}
          placement="bottom"
        >
          <PopoverBody>
            <ReactColor
              onChange={this.onChange}
              color={value}
            />
          </PopoverBody>
        </Popover>
      </span>
    )
  }
}

export default Module;
