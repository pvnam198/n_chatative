/* eslint-disable jsx-a11y/alt-text */
import React, { Component } from 'react';
import { Tooltip } from 'reactstrap';
import BgPosition from './../../../../assets/img/preview/position-preview.png'
import './index.css';

// 18 218
function calculatePosition(position) {
  var width = 200;
  var widthWindow = 60;
  var widthButton = 20;
  var marginLeft = 18;
  var leftWidow = width * position / 100 + marginLeft ;
  var leftButton = width * position / 100 + marginLeft ;
  if (position > 50 ) {
    leftWidow = leftWidow - marginLeft;
    leftButton = leftWidow + widthWindow - widthButton;
  }
  return {
    leftButton,
    leftWidow,
  }
}
class PreviewPosition extends Component {
  render() {
    const { target, isOpen = false, position } = this.props;
    const left = calculatePosition(position);
    return (
      <Tooltip target={target} isOpen={isOpen}>
        <img src={BgPosition} />
        <div id="window_chat" style={{ left: left.leftWidow }}></div>
        <div id="button_chat" style={{ left: left.leftButton }} ></div>
      </Tooltip>
    );
  }
}

PreviewPosition.propTypes = {

};
PreviewPosition.defaultProps = {

};

export default PreviewPosition;
