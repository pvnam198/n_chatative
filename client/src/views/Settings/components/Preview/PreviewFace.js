/* eslint-disable jsx-a11y/alt-text */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import './preview.css';

import Chatbox from './../../../../assets/img/preview/chat_box.png';
import ChatboxLoggedIn from './../../../../assets/img/preview/chatbox_logged_in.png';
import ChatboxLoggedOut from './../../../../assets/img/preview/chatbox_logged_out.png';

import ButtonPreview from './ButtonPreview';

class Module extends Component {
  render() {
    const { settings, preview } = this.props;
    const { messageLoggedIn, messageLoggedOut, bubbleColor } = settings.data;
    const { hasShowLoggedIn, hasShowLoggedOut } = preview;
    let text = '';
    let srcBg = Chatbox;
    if (hasShowLoggedIn) {
      text = messageLoggedIn;
      srcBg = ChatboxLoggedIn;
    }
    if (hasShowLoggedOut) {
      text = messageLoggedOut;
      srcBg = ChatboxLoggedOut;
    }
    return (
      <div className="bg-preview">
        <span className="greeting">{text}</span>
        <div style={{ backgroundColor: bubbleColor, borderRadius: 40 }}>
          <img src={srcBg} width={382} />
        </div>
        <div className="fb-button-preview pull-right">
          <ButtonPreview settings={settings.data} />
        </div>
      </div>
    );
  }
}

Module.propTypes = {

};
Module.defaultProps = {

};

const mapStateToProps = state => ({
  settings: state.settings,
  preview: state.preview,
});

export default connect(mapStateToProps)(Module);
