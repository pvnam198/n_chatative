/* eslint-disable jsx-a11y/alt-text */
import React from 'react';


const ButtonPreview = (props) => {
  const { settings } = props;
  const { isDefault, iconButton, textButton, buttonColor, iconTextColor } = settings;
  if (isDefault) {
    return (
      <div className="btn-default">
        <img src='static/img/btn/messenger_icon.png' />
      </div>
    )
  }

  return (
    <div className="btn-fb-custom" style={{ backgroundColor: buttonColor, color: iconTextColor }}>
      {/*<img src={`static/img/btn/${iconOnButton}.svg`} />*/}
      <span className={`icon ${iconButton}`}></span>
      {textButton && <span className="icon-text-custom">{textButton}</span>}
    </div>
  )

};

export default ButtonPreview;
