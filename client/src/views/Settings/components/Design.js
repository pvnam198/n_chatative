import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Card, CardHeader, CardBody, FormGroup, Label, Input } from 'reactstrap'
import DesignCustom from './DesignCustom';
import * as Actions from "../../../redux/actions";

class Design extends Component {
  onChangeDefault = () => {
    this.props.onChangeCustom(!this.props.settings.data.isDefault);
  };
  render() {
    const {
      settings,
      onChangeIconButton,
      onChangeTextButton,
      onChangeColorButton,
      onChangeColorText,
      onChangeColorBubble,
    } = this.props;
    const { isDefault } = settings.data;
    return (
      <Card>
        <CardHeader>
          <strong>Design</strong>
        </CardHeader>
        <CardBody>
          <FormGroup check>
            <Label check>
              <Input type="radio" name="radio2" onChange={this.onChangeDefault} checked={isDefault} />{' '}
              Use Facebook's default schema
            </Label>
          </FormGroup>
          <FormGroup check>
            <Label check>
              <Input type="radio" name="radio2" onChange={this.onChangeDefault} checked={!isDefault} />{' '}
              Use custom design for Chatbox
            </Label>
          </FormGroup>
          {
            !isDefault &&
            <DesignCustom
              settings={settings.data}
              onChangeIconButton={onChangeIconButton}
              onChangeTextButton={onChangeTextButton}
              onChangeColorButton={onChangeColorButton}
              onChangeColorText={onChangeColorText}
              onChangeColorBubble={onChangeColorBubble}
            />
          }
        </CardBody>
      </Card>
    );
  }
}

Design.propTypes = {

};
Design.defaultProps = {

};

const mapStateToProps = state => ({
  settings: state.settings,
});

const mapDispatchToProps = dispatch => ({
  onChangeCustom: value => dispatch(Actions.chatboxCustomChange(value)),
  onChangeIconButton: value => dispatch(Actions.iconButtonChange(value)),
  onChangeTextButton: value => dispatch(Actions.textOnButtonChange(value)),
  onChangeColorButton: value => dispatch(Actions.buttonColorChange(value)),
  onChangeColorText: value => dispatch(Actions.textColorChange(value)),
  onChangeColorBubble: value => dispatch(Actions.bubbleColorChange(value)),
});
export default connect(mapStateToProps, mapDispatchToProps)(Design);


