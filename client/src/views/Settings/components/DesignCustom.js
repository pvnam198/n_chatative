import React, { Component } from 'react';
import { Row, Col, FormGroup, Label, Input } from 'reactstrap'
import InputColor from './InputColor';
import ButtonIcon from './ButtonIcon/ButtonIcon';
import * as Constants from './../../../constants';

export const Icons = [Constants.ICON_1, Constants.ICON_2, Constants.ICON_3, Constants.ICON_4,
  Constants.ICON_5, Constants.ICON_6, Constants.ICON_7, Constants.ICON_8];

class DesignCustom extends Component {
  onChangeTextButton = (e) => {
    this.props.onChangeTextButton(e.currentTarget.value);
  };
  render() {
    const {
      settings,
      onChangeIconButton,
      onChangeColorButton,
      onChangeColorText,
      onChangeColorBubble,
    } = this.props;
    const {
      iconButton,
      textButton,
      buttonColor,
      iconTextColor,
      bubbleColor,
    } = settings;
    return (
      <div>
        <FormGroup>
          <Label htmlFor="icon_button"><strong>Icon on button</strong></Label>
          <div>
            <div
              id="icon_button"
            >
              {Icons.map((item, index) => (
                <ButtonIcon
                  icon={item}
                  active={iconButton}
                  onClick={onChangeIconButton}
                  key={`icon-${index}`}
                  size="sm"
                />
              ))}
            </div>
          </div>
        </FormGroup>
        <FormGroup>
          <Label htmlFor="text_message"><strong>Text on button</strong></Label>
          <Input
            type="text"
            id="text_message"
            required
            onChange={this.onChangeTextButton}
            value={textButton}
          />
        </FormGroup>
        <Row>
          <Col>
            <FormGroup>
              <Label htmlFor="bubble_color"><strong>Bubble color</strong></Label>
              <InputColor
                id="bubble_color"
                onChange={onChangeColorBubble}
                value={bubbleColor}
              />
            </FormGroup>
          </Col>
          <Col>
            <FormGroup>
              <Label htmlFor="button_color"><strong>Button color</strong></Label>
              <InputColor
                id="button_color"
                onChange={onChangeColorButton}
                value={buttonColor}
              />
            </FormGroup>
          </Col>
        </Row>
        <Row>
          <Col>
            <FormGroup>
              <Label htmlFor="text_color"><strong>Text & Icon color</strong></Label>
              <InputColor
                id="text_color"
                onChange={onChangeColorText}
                value={iconTextColor}
              />
            </FormGroup>
          </Col>
          <Col />
        </Row>
      </div>
    );
  }
}

DesignCustom.propTypes = {

};
DesignCustom.defaultProps = {

};

export default DesignCustom;
