import Settings from './views/Settings';

const routes = [
  { path: '/', exact: true, name: 'Settings', component: Settings },
];

export default routes;
