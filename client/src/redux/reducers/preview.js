import * as Action_Types from './../../constants/action-types';

const initState = {
  hasShowLoggedIn: false,
  hasShowLoggedOut: false,
};
export default function users(state = initState, action) {
  const { type } = action;
  switch (type) {
    case Action_Types.SETTINGS_GREETING_LOGGED_OUT_FOCUS:
      return {
        ...state,
        hasShowLoggedOut: true,
      };
    case Action_Types.SETTINGS_GREETING_LOGGED_OUT_BLUR:
      return {
        ...state,
        hasShowLoggedOut: false,
      };
    case Action_Types.SETTINGS_GREETING_LOGGED_IN_FOCUS:
      return {
        ...state,
        hasShowLoggedIn: true,
      };
    case Action_Types.SETTINGS_GREETING_LOGGED_IN_BLUR:
      return {
        ...state,
        hasShowLoggedIn: false,
      };
    default:
      return state;
  }
}
