import * as Action_Types from './../../constants/action-types';
import * as Icons from './../../constants/index';

const initState = {
  isFetching: false,
  data: {
    messageLoggedIn: 'Hi, How may I help you',
    messageLoggedOut: 'Hello, Well come to my shop',
    isDefault: false,
    iconButton: Icons.ICON_1,
    textButton: 'Send a message',
    buttonColor: '#7FB9EE',
    iconTextColor: '#ffffff',
    bubbleColor: '#FFFFFF',
    position: 100,
    pageUrl: '',
    showDesktop: true,
    showMobile: true,
    language: 'en_US',
  }
};

const defaultSettings = {
  messageLoggedIn: 'Hi, How may I help you',
  messageLoggedOut: 'Hello, Well come to my shop',
  isDefault: false,
  iconButton: Icons.ICON_1,
  textButton: 'Send a message',
  buttonColor: '#7FB9EE',
  iconTextColor: '#ffffff',
  bubbleColor: '#FFFFFF',
  position: 100,
  pageUrl: '',
  showDesktop: true,
  showMobile: true,
  language: 'en_US',
};

export default function users(state = initState, action) {
  const { type, payload } = action;
  switch (type) {
    case Action_Types.SETTINGS_RETRIEVE_REQUESTED:
      return {
        ...state,
        isFetching: true,
      };
    case Action_Types.SETTINGS_RETRIEVE_SUCCESS:
      return {
        ...state,
        data: { ...payload.data },
        isFetching: false,
      };
    case Action_Types.SETTINGS_RETRIEVE_FAILED:
      return {
        ...state,
        isFetching: false,
      };
    case Action_Types.SETTINGS_GREETING_LOGGED_IN_CHANGE:
      return {
        ...state,
        data: {
          ...state.data,
          messageLoggedIn: payload,
        }
      };
    case Action_Types.SETTINGS_GREETING_LOGGED_OUT_CHANGE:
      return {
        ...state,
        data: {
          ...state.data,
          messageLoggedOut: payload,
        }
      };
    case Action_Types.SETTINGS_CUSTOM_DEFAULT_CHANGE:
      return {
        ...state,
        data: {
          ...state.data,
          isDefault: payload,
        }
      };
    case Action_Types.SETTINGS_ICON_BUTTON_CHANGE:
      return {
        ...state,
        data: {
          ...state.data,
          iconButton: payload,
        }
      };
    case Action_Types.SETTINGS_TEXT_BUTTON_CHANGE:
      return {
        ...state,
        data: {
          ...state.data,
          textButton: payload,
        }
      };
    case Action_Types.SETTINGS_BUTTON_COLOR_CHANGE:
      return {
        ...state,
        data: {
          ...state.data,
          buttonColor: payload,
        }
      };
    case Action_Types.SETTINGS_TEXT_BUTTON_COLOR_CHANGE:
      return {
        ...state,
        data: {
          ...state.data,
          iconTextColor: payload,
        }
      };
    case Action_Types.SETTINGS_BUBBLE_COLOR_CHANGE:
      return {
        ...state,
        data: {
          ...state.data,
          bubbleColor: payload,
        }
      };
    case Action_Types.SETTINGS_PAGE_URL_CHANGE:
      return {
        ...state,
        data: {
          ...state.data,
          pageUrl: payload,
        }
      };
    case Action_Types.SETTINGS_POSITION_CHANGE:
      return {
        ...state,
        data: {
          ...state.data,
          position: payload,
        }
      };

    case Action_Types.SETTINGS_RESET_DEFAULT:
      return {
        ...state,
        data: defaultSettings,
      };
    case Action_Types.SETTINGS_SHOW_DESKTOP_CHANGE:
      return {
        ...state,
        data: {
          ...state.data,
          showDesktop: payload,
        }
      };
    case Action_Types.SETTINGS_SHOW_MOBILE_CHANGE:
      return {
        ...state,
        data: {
          ...state.data,
          showMobile: payload,
        }
      };
    case Action_Types.SETTINGS_LANGUAGE_CHANGE:
      return {
        ...state,
        data: {
          ...state.data,
          language: payload,
        }
      };

    default:
      return state;
  }
}
