import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import settings from './settings';

import user from './user';
import preview from './preview';

const appReducer = combineReducers({
  router: routerReducer,
  user,
  settings,
  preview,
});

const rootReducer = (state, action) => appReducer(state, action);

export default rootReducer;
