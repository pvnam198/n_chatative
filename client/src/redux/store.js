import { createStore, applyMiddleware, compose } from 'redux';
import { routerMiddleware } from 'react-router-redux';
import thunkMiddleware from 'redux-thunk';
import humps from 'humps';
import { multiClientMiddleware } from 'redux-axios-middleware';
import axios from 'axios';
import rootReducer from './reducers';
import { API_ROOT } from './../constants'

const apiClients = {
  shop: {
    client: axios.create({
      baseURL: API_ROOT,
      headers: {
        'Authorization': `Token ${window.fitiToken}`,
        // 'Authorization': `Token 5c198d21d4509fb8e214f9178baae7dc`,
        // 'Content-Type': 'application/json',
        // 'Accept': 'application/json',
      },
      transformResponse: [
        data => humps.camelizeKeys(JSON.parse(data)),
      ],
    }),
  }
};


export default ({ initialState = {}, history }) => {

  const __DEVELOPMENT__ = process.env.NODE_ENV !== 'production';

  const middlewares = [
    // promiseMiddleware(),
    thunkMiddleware,
    routerMiddleware(history),
    multiClientMiddleware(apiClients),
  ];

  let enhancers = [applyMiddleware(...middlewares)];

  if (__DEVELOPMENT__ && window.devToolsExtension) {
    const { persistState } = require('redux-devtools');
    enhancers = [
      ...enhancers,
      window.devToolsExtension(),
      persistState(window.location.href.match(/[?&]debug_session=([^&]+)\b/))
    ];
  }

  const finalCreateStore = compose(...enhancers)(createStore);

  const store = finalCreateStore(
    rootReducer,
    initialState,
  );

  return store;
};
