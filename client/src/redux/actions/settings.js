import humps from 'humps';
import * as Action_Types from './../../constants/action-types';

export const greetingLoggedInChange = (value) => ({
  type: Action_Types.SETTINGS_GREETING_LOGGED_IN_CHANGE,
  payload: value,
});

export const greetingLoggedOutChange = (value) => ({
  type: Action_Types.SETTINGS_GREETING_LOGGED_OUT_CHANGE,
  payload: value,
});

export const greetingLoggedOutFocus = (value = true) => ({
  type: Action_Types.SETTINGS_GREETING_LOGGED_OUT_FOCUS,
  payload: value,
});

export const greetingLoggedOutBlur = (value = true) => ({
  type: Action_Types.SETTINGS_GREETING_LOGGED_OUT_BLUR,
  payload: value,
});

export const greetingLoggedInFocus = (value = true) => ({
  type: Action_Types.SETTINGS_GREETING_LOGGED_IN_FOCUS,
  payload: value,
});

export const greetingLoggedInBlur = (value = true) => ({
  type: Action_Types.SETTINGS_GREETING_LOGGED_IN_BLUR,
  payload: value,
});

export const chatboxCustomChange = (custom = true) => ({
  type: Action_Types.SETTINGS_CUSTOM_DEFAULT_CHANGE,
  payload: custom,
});

export const iconButtonChange = (icon) => ({
  type: Action_Types.SETTINGS_ICON_BUTTON_CHANGE,
  payload: icon,
});


export const textOnButtonChange = (value) => ({
  type: Action_Types.SETTINGS_TEXT_BUTTON_CHANGE,
  payload: value,
});

export const buttonColorChange = (value) => ({
  type: Action_Types.SETTINGS_BUTTON_COLOR_CHANGE,
  payload: value,
});
export const textColorChange = (value) => ({
  type: Action_Types.SETTINGS_TEXT_BUTTON_COLOR_CHANGE,
  payload: value,
});
export const bubbleColorChange = (value) => ({
  type: Action_Types.SETTINGS_BUBBLE_COLOR_CHANGE,
  payload: value,
});

export const pageUrlChange = (value) => ({
  type: Action_Types.SETTINGS_PAGE_URL_CHANGE,
  payload: value,
});

export const changePosition = (value) => ({
  type: Action_Types.SETTINGS_POSITION_CHANGE,
  payload: value,
});

export const updateSettings = (body) => ({
  types: [
    Action_Types.SETTINGS_UPDATE_REQUESTED,
    Action_Types.SETTINGS_UPDATE_SUCCESS,
    Action_Types.SETTINGS_UPDATE_FAILED,
  ],
  payload: {
    client: 'shop',
    request: {
      method: 'PATCH',
      url: `/config/`,
      data: humps.decamelizeKeys(body),
    },
  },
});

export const getSettings = () => ({
  types: [
    Action_Types.SETTINGS_RETRIEVE_REQUESTED,
    Action_Types.SETTINGS_RETRIEVE_SUCCESS,
    Action_Types.SETTINGS_RETRIEVE_FAILED,
  ],
  payload: {
    client: 'shop',
    request: {
      method: 'GET',
      url: `/config/`,
    },
  },
});

export const resetDefault = () => ({
  type: Action_Types.SETTINGS_RESET_DEFAULT,
  payload: '',
});

export const changeShowMobile = (value) => ({
  type: Action_Types.SETTINGS_SHOW_MOBILE_CHANGE,
  payload: value,
});
export const changeShowDesktop = (value) => ({
  type: Action_Types.SETTINGS_SHOW_DESKTOP_CHANGE,
  payload: value,
});

export const changeLanguage = value => ({
  type: Action_Types.SETTINGS_LANGUAGE_CHANGE,
  payload: value,
});
