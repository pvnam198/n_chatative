import * as Action_Types from './../../constants/action-types';

export const getUser = () => ({
  types: [
    Action_Types.SHOP_RETRIEVE_REQUESTED,
    Action_Types.SHOP_RETRIEVE_SUCCESS,
    Action_Types.SHOP_RETRIEVE_FAILED,
  ],
  payload: {
    client: 'shop',
    request: {
      method: 'GET',
      url: `/shop/`,
    },
  },
});
