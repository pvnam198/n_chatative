export const API_ROOT = process.env.REACT_APP_API_ROOT;
export const URL_ROOT = process.env.REACT_APP_URL_ROOT;
export const URL_HELP = process.env.REACT_APP_URL_HELP;
export const URL_LOGOUT_REDIRECT = process.env.REACT_APP_URL_LOGOUT_REDIRECT;

export const ICON_1 = 'icon-1';
export const ICON_2 = 'icon-2';
export const ICON_3 = 'icon-3';
export const ICON_4 = 'icon-4';
export const ICON_5 = 'icon-5';
export const ICON_6 = 'icon-6';
export const ICON_7 = 'icon-7';
export const ICON_8 = 'icon-8';
