import './polyfill'
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import { Provider } from 'react-redux';
import createHistory from 'history/createHashHistory';
import configureStore from './redux/store';

const initialState = {};
const history = createHistory();
const store = configureStore({ initialState, history });

ReactDOM.render(
  (<Provider store={store}>
    <App />
  </Provider>)
    , document.getElementById('root'));

