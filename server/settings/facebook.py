from .base import config

FACEBOOK_APP_ID = config.get('facebook_app', 'facebook_app_id')
FACEBOOK_APP_SECRET = config.get('facebook_app', 'facebook_app_secret')
