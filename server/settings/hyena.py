from .base import config

HYENA_ROOT_URL = config.get("hyena", "hyena_root_url")
HYENA_KEY = config.get("hyena", "hyena_key")