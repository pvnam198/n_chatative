from .base import config

# https://github.com/cipriantarta/django-logging
DJANGO_LOGGING = {
    "CONSOLE_LOG": False,
    "LOG_PATH": config.get("apps", "logfile_root")
}