from .base import config

# Configure Shopify Application settings
SHOPIFY_APP_NAME = config.get('shopify', 'app_name')
SHOPIFY_APP_API_KEY = config.get('shopify', 'api_key')
SHOPIFY_APP_API_SECRET = config.get('shopify', 'api_secret')
SHOPIFY_APP_API_SCOPE = config.get('shopify', 'api_scope').split(',')
SHOPIFY_APP_IS_EMBEDDED = config.getboolean('shopify', 'is_embedded')
SHOPIFY_APP_DEV_MODE = config.getboolean('shopify', 'dev_mode')

# Use the Shopify Auth authentication backend as the sole authentication backend.
AUTHENTICATION_BACKENDS = (
    'shopify_auth.backends.ShopUserBackend',
)

# Use the Shopify Auth user model.
AUTH_USER_MODEL = 'chatative.ShopUser'

# Set the login redirect URL to the "home" page for your app (where to go after logging on).
LOGIN_REDIRECT_URL = '/'
LOGIN_URL = '/login/'

# Set secure proxy header to allow proper detection of secure URLs behind a proxy.
# This ensures that correct 'https' URLs are generated when our Django app is running behind a proxy like nginx, or is
# being tunneled (by ngrok, for example).
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')
