from .base import config

REDIS_HOST = config.get("redis", "host")
REDIS_PORT = config.get("redis", "port")

REDIS_CACHE_TIMEOUT = int(config.get("redis", "redis_cache_timeout"))
REDIS_CACHE_TIMEOUT_DEFAULT = int(config.get("redis", "redis_cache_timeout_default"))
