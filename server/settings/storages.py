from .base import config

DEFAULT_FILE_STORAGE = config.get('aws_s3', 'default_file_storage')
AWS_ACCESS_KEY_ID = config.get('aws_s3', 'aws_access_key_id')
AWS_SECRET_ACCESS_KEY = config.get('aws_s3', 'aws_secret_access_key')
AWS_STORAGE_BUCKET_NAME = config.get('aws_s3', 'aws_storage_bucket_name')
AWS_QUERYSTRING_AUTH = config.getboolean('aws_s3', 'aws_querystring_auth')
