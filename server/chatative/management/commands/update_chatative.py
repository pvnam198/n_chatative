from django.core.management import BaseCommand
from chatative.models import CustomChatBox


class Command(BaseCommand):

    def handle(self, *args, **options):
        list_custom_chat = CustomChatBox.objects.all()
        for item in list_custom_chat:
            item.save()
