from __future__ import unicode_literals

from django.conf.urls import url, include
from .views import DashboardView, LogoutView
from .apis import urls as api_url

urlpatterns = [
    url(r'^$', DashboardView.as_view()),
    url(r'^logout/$', LogoutView.as_view()),
    url(r'^api/', include(api_url)),
]
