from __future__ import unicode_literals

from django.conf.urls import url
from .views import ShopView, CustomChatBoxView, ShopUpdateView, UninstallView

urlpatterns = [
    url(r'^me/$', ShopView.as_view(), name='api-shop'),
    url(r'^config/$', CustomChatBoxView.as_view(), name='api-custom-chatbox'),
    url(r'^hooks/uninstall/$', UninstallView.as_view(), name='api-uninstall'),
    url(r'^hooks/shop/$', ShopUpdateView.as_view(), name='api-shop-update'),
]
