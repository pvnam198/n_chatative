from __future__ import unicode_literals

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

from chatative.models import CustomChatBox
from .authentications import TokenAuthentication
from .serializers import CustomChatBoxSerializer
import shopify
import logging

logger = logging.getLogger(__name__)


class ShopView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        session = shopify.Session(request.user.myshopify_domain, request.user.token)
        shopify.ShopifyResource.activate_session(session)
        shop_info = shopify.Shop.current()
        return Response(shop_info.to_dict())


class CustomChatBoxView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = CustomChatBoxSerializer

    def get(self, request, *args, **kwargs):
        custom_chatbox, _ = CustomChatBox.objects.get_or_create(shop=request.user)
        serializer = self.serializer_class(custom_chatbox)
        return Response(serializer.data)

    def patch(self, request, *args, **kwargs) :
        custom_chatbox, _ = CustomChatBox.objects.get_or_create(shop=request.user)
        serializer = self.serializer_class(custom_chatbox, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)


class UninstallView(APIView):

    def post(self, request, *args, **kwargs):
        return Response({"status": "success"})


class ShopUpdateView(APIView):

    def post(self, request, *args, **kwargs):
        return Response({"status": "success"})
