from __future__ import unicode_literals

from rest_framework.serializers import ModelSerializer
from ..models import CustomChatBox


class CustomChatBoxSerializer(ModelSerializer):
    class Meta:
        model = CustomChatBox
        fields = '__all__'
        read_only_fields = ('page_id',)
