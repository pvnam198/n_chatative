from __future__ import unicode_literals

from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver
from django.template.loader import render_to_string
from shopify_auth.models import AbstractShopUser
from django.db import models
from django.core.files.storage import default_storage
import logging
import json
import re
import requests
import shopify
import time

logger = logging.getLogger(__name__)


class ShopUser(AbstractShopUser):
    name = models.CharField(max_length=250, null=True, blank=True)
    email = models.EmailField(null=True, blank=True)
    phone = models.CharField(max_length=50, null=True, blank=True)
    extra_data = models.TextField(null=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True)
    created_at = models.DateTimeField(auto_now_add=True)


class ScriptTag(models.Model):
    shop = models.OneToOneField(ShopUser)
    script_id = models.TextField(max_length=50)
    src = models.CharField(max_length=500)
    version = models.CharField(max_length=50)
    extra_data = models.TextField(null=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True)
    created_at = models.DateTimeField(auto_now_add=True)


class CustomChatBox(models.Model):
    shop = models.OneToOneField(ShopUser)
    page_url = models.CharField(max_length=500, default='', blank=True)
    page_id = models.CharField(max_length=50, default='')
    message_logged_in = models.CharField(max_length=500, default='Hi, How may I help you')
    message_logged_out = models.CharField(max_length=500, default='Hello, Well come to my shop')
    is_default = models.BooleanField(default=False)
    font_size = models.IntegerField(default=18)
    language = models.CharField(max_length=50, default='en_US')
    show_mobile = models.BooleanField(default=True)
    show_desktop = models.BooleanField(default=True)
    icon_button = models.CharField(max_length=50, default='icon-1')
    text_button = models.CharField(max_length=150, default='Send a message', blank=True)
    button_color = models.CharField(max_length=50, default='#7FB9EE')
    icon_text_color = models.CharField(max_length=50, default='#ffffff')
    bubble_color = models.CharField(max_length=50, default='#FF007B')
    position = models.IntegerField(default=90)

    def get_page_id(self, link):
        response = requests.get(link)
        result = response.text
        page_id = re.search(r"page_id=[0-9]+", result)
        page_id = re.search(r"[0-9]+", page_id.group()).group()
        return page_id


@receiver(pre_save, sender=CustomChatBox)
def update_page_id(sender, instance, *args, **kwargs):
    if not instance.page_url:
        instance.page_id = ''
    else:
        if instance.id is None:
            instance.page_id = instance.get_page_id(instance.page_url)
        else:
            old_instance = CustomChatBox.objects.get(id=instance.id)
            if instance.page_url != old_instance.page_url:
                instance.page_id = instance.get_page_id(instance.page_url)


@receiver(post_save, sender=CustomChatBox)
def save_custom_chatbox(sender, instance, **kwargs):
    if not instance.page_id:
        return

    shop = instance.shop
    session = shopify.Session(shop.myshopify_domain, shop.token)
    shopify.ShopifyResource.activate_session(session)

    chatative_template = render_to_string('chatative.js', {
        'page_id': instance.page_id,
        'message_logged_in': instance.message_logged_in,
        'message_logged_out': instance.message_logged_out,
        'is_default': instance.is_default,
        'icon_button': instance.icon_button,
        'text_button': instance.text_button,
        'button_color': instance.button_color,
        'icon_text_color': instance.icon_text_color,
        'bubble_color': instance.bubble_color,
        'position': instance.position,
        'show_mobile': instance.show_mobile,
        'show_desktop': instance.show_desktop,
        'language': instance.language,
    })

    new_version = str(int(time.time()))
    chatative_filename = 'chatative/chatative.%s.%s.js' % (str(shop.id), str(int(time.time())))
    chatative_file = default_storage.open(chatative_filename, 'w')
    chatative_file.write(chatative_template)
    chatative_file.close()
    chatative_url = default_storage.url(chatative_filename)

    try:
        shop_script_tag = shop.scripttag
        script_tag = shopify.ScriptTag.find_first(id=shop_script_tag.id)
        if script_tag:
            script_tag.src = chatative_url
            script_tag.save()
        else:
            script_tag = shopify.ScriptTag.create({
                'event': 'onload',
                'src': chatative_url,
            })
        shop_script_tag.script_id = script_tag.id
        shop_script_tag.src = script_tag.src
        shop_script_tag.version = new_version
        shop_script_tag.extra_data = json.dumps(script_tag.to_dict())
        shop_script_tag.save()
    except ScriptTag.DoesNotExist:
        script_tag = shopify.ScriptTag.create({
            'event': 'onload',
            'src': chatative_url,
        })
        ScriptTag.objects.create(
            shop=shop,
            script_id=script_tag.id,
            src=script_tag.src,
            version=new_version,
            extra_data=json.dumps(script_tag.to_dict())
        )
    except Exception as ex:
        print(ex)
        logger.error(ex)
