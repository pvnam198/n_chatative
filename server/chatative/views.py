from __future__ import unicode_literals

import shopify
from django.contrib.auth.mixins import AccessMixin
from django.contrib.auth import logout
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from django.utils.decorators import method_decorator
from shopify_auth.decorators import login_required
from django.views.generic import View
from django.conf import settings
from .models import ShopUser
import logging
import json

logger = logging.getLogger(__name__)


class LoginRequiredMixin(AccessMixin):

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(LoginRequiredMixin, self).dispatch(*args, **kwargs)


class ShopifyBase(object):
    shop_info = None

    def dispatch(self, request, *args, **kwargs):
        try:
            session = shopify.Session(request.user.myshopify_domain, request.user.token)
            shopify.ShopifyResource.activate_session(session)
            self.shop_info = shopify.Shop.current()
            ShopUser.objects.filter(id=request.user.id).update(
                name=self.shop_info.name,
                email=self.shop_info.email,
                phone=self.shop_info.phone,
                extra_data=json.dumps(self.shop_info.to_dict())
            )
            return super(ShopifyBase, self).dispatch(request, *args, **kwargs)
        except Exception as ex:
            print(ex)
            logger.error(ex)
            return redirect('logout/')

    def get_all_resources(self, resource, **kwargs):
        resource_count = resource.count(**kwargs)
        resources = []
        if resource_count > 0:
            for page in range(1, ((resource_count-1) // 250) + 2):
                kwargs.update({'limit': 250, 'page': page})
                resources.extend(resource.find(**kwargs))
        return resources

    def create_web_hook(self, topic, address):
        return shopify.Webhook.create({
            'topic': topic,
            'address': address,
            'format': 'json'
        })

    def create_script_tag(self, src, event='onload'):
        return shopify.ScriptTag.create({
            'event': event,
            'src': src,
        })


class DashboardView(LoginRequiredMixin, ShopifyBase, View):

    def get(self, request, *args, **kwargs):
        context = {
            'access_token': request.user.token,
            'shop_info': json.dumps(self.shop_info.to_dict())
        }
        return render(request, 'dashboard.html', context)


class LogoutView(View):

    def get(self, request, *args, **kwargs):
        shopify.ShopifyResource.clear_session()
        logout(request)
        next_page = request.GET.get('next', settings.LOGIN_URL)
        return HttpResponseRedirect(next_page)
