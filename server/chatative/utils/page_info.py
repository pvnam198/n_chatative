from __future__ import unicode_literals

from django.conf import settings
import facebook

FACEBOOK_APP_ID = settings.FACEBOOK_APP_ID
FACEBOOK_APP_SECRET = settings.FACEBOOK_APP_SECRET


graph = facebook.GraphAPI(version='2.7')
graph.access_token = graph.get_app_access_token(settings.FACEBOOK_APP_ID, settings.FACEBOOK_APP_SECRET)

page_id = graph.get_connections(id='softviet.net', connection_name='id')
print(page_id)
