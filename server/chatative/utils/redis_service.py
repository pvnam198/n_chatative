from __future__ import unicode_literals

from redis.client import Redis
from settings import REDIS_HOST, REDIS_PORT, REDIS_CACHE_TIMEOUT_DEFAULT
import time

redis_conf = {
    'host': REDIS_HOST,
    'port': REDIS_PORT,
    'db': 0
}

redis_instance = Redis(**redis_conf)


class RedisClient(object):

    def build_key_redis(self, type, id, has_timeout=False):
        if has_timeout:
            return str(type) + '_timeout_' + str(id)
        return str(type) + '_' + str(id)

    def set(self, name, value, timeout=REDIS_CACHE_TIMEOUT_DEFAULT):
        """
        :param name:
        :param value:
        :param timeout: TIMEOUT_DEFAULT seconds
        :return:
        """
        redis_instance.set(name=name, value=value, ex=timeout)

    def get(self, name):
        return redis_instance.get(name)

    def set_cache(self, type_object, id, value):
        key_obj = self.build_key_redis(type_object, id)
        key_timeout_obj = self.build_key_redis(type_object, id, True)
        time_now = time.time()
        self.set(key_obj, value)
        self.set(key_timeout_obj, time_now)

