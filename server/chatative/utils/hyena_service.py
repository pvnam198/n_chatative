from __future__ import unicode_literals

from django.conf import settings
import requests


def setup_hyena_sdk(domain, token):
    data = {
        'domain': domain,
        'token': token
    }
    header = {
        'Authorization': settings.HYENA_KEY,
        'Content-Type': 'application/json',
    }
    return requests.post(url=settings.HYENA_ROOT_URL, data=data, headers=header)
