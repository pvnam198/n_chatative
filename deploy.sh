#!/usr/bin/env bash

echo "Start build dashboard"
cd ./client
yarn install && yarn build
while :
do
	if [ -d "./build/static" ]; then break ; fi
	printf ".";
	sleep 2;
done
mkdir -p ../server/static/img && rsync -av --delete ./build/static/img/ ../server/static/img
mkdir -p ../server/static/css && rsync -av --delete ./build/static/css/ ../server/static/css
mkdir -p ../server/static/js && rsync -av --delete ./build/static/js/ ../server/static/js
mkdir -p ../server/static/media && rsync -av --delete ./build/static/media/ ../server/static/media
cp ./build/static/favicon.ico ../server/static/
cp ./build/index.html ../server/chatative/templates/dashboard.html

echo "Start build script for shop"
cd ../client-shop/
yarn install && yarn build
cp ./dist/index.js ../server/chatative/templates/chatative.js

echo "Deploy Chatative service"
service chatative restart

echo "Completed!"
