1. Copy file chatative.service to /etc/systemd/system/
sudo systemctl start chatative
sudo systemctl enable chatative
sudo systemctl status chatative

2. Copy file chatative to /etc/nginx/sites-available/
sudo ln -s /etc/nginx/sites-available/chatative /etc/nginx/sites-enabled
sudo nginx -t
sudo systemctl restart nginx
