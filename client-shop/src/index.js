
const styleButton = `
.btn-fb-custom {
  border-radius: 30px;
  background-color: #00aced;
  padding-left: 16px;
  padding-right: 16px;
  height: 40px;
  display: inline-flex;
  align-items: center;
  cursor: pointer;
  position: fixed;
  bottom: 18pt;
  z-index: 100000;
  box-shadow: 0 8px 4px -7px #777;
}
.fb_dialog .fb_dialog_advanced {

}
.btn-fb-custom span.icon-custom {
  font-size: 22px;
  display: inline-block;
  speak: none;
  text-transform: none;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  margin-bottom: 0;
  font-family: qfc;
}
.btn-fb-custom span.icon-custom-text {
    margin-left: 10px;
}
.btn-fb-custom span.icon-1::before {
  content: "\\ea03";
}
.btn-fb-custom span.icon-2::before {
  content: "\\ea04";
}
.btn-fb-custom span.icon-3::before {
  content: "\\ea05";
}
.btn-fb-custom span.icon-4::before {
  content: "\\ea06";
}
.btn-fb-custom span.icon-5::before {
  content: "\\ea07";
}
.btn-fb-custom span.icon-6::before {
  content: "\\ea08";
}
.btn-fb-custom span.icon-7::before {
  content: "\\ea09";
}
.btn-fb-custom span.icon-8::before {
  content: "\\ea0a";
}

.btn-fb-custom span {
  font-size: 18px;
}

.fb_dialog.fb_dialog_advanced, .fb_dialog.fb_dialog_mobile {
    opacity: 0.001 !important;
}

@media (max-width: 480px) {
  .btn-fb-custom {
    border-radius: 50%;
    padding-left: 12px;
    padding-right: 12px;
    height: 54px;
  }
  .btn-fb-custom span.icon-custom {
    font-size: 30px;
    
  }
}

`;
/*
 const settings = {
    loggedInGreeting: 'Hi, I help you',
    loggedOutGreeting: 'Hello, Well come to my shop',
    useDefault: false,
    iconOnButton: 'icon-1',
    textOnButton: 'Send a message',
    buttonColor: '#7FB9EE',
    iconTextColor: '#ffffff',
    bubbleColor: '#FF007B',
    buttonPosition: 30,
     showDesktop: true,
     showMobile: true,
     language: 'vi_VN',
};
const pageId = '350907291625441';
*/

const settings = {
    loggedInGreeting: '{{ message_logged_in }}',
    loggedOutGreeting: '{{ message_logged_out }}',
    useDefault: '{{ is_default }}'.toLowerCase() === 'true',
    iconOnButton: '{{ icon_button }}',
    textOnButton: '{{ text_button }}',
    buttonColor: '{{ button_color }}',
    iconTextColor: '{{ icon_text_color }}',
    bubbleColor: '{{ bubble_color }}',
    buttonPosition: parseInt('{{ position }}'),
    showDesktop: '{{ show_desktop }}'.toLowerCase() === 'true',
    showMobile: '{{ show_mobile }}'.toLowerCase() === 'true',
    language: '{{ language }}',
};

const pageId = '{{ page_id }}';


const urlFont = "https://s3-us-west-1.amazonaws.com/fitivn/chatative/qfc.woff2";

function refreshFrameFacebook() {
    if (settings.useDefault) return;
    var buttonDialog =  document.body.querySelector('.fb_dialog.fb_dialog_advanced');
    if (isMobile()) buttonDialog =  document.body.querySelector('.fb_dialog.fb_dialog_mobile');
    var customButton = document.body.querySelector('.btn-fb-custom');
    var offsetLeft = customButton.offsetLeft;
    var clientWidth = customButton.clientWidth;
    // set button style
    buttonDialog.style.opacity = 0.001;
    buttonDialog.style.width = clientWidth + 'px';
    buttonDialog.style.left = offsetLeft + 'px';
    var iframeButton = buttonDialog.querySelector('iframe');
    iframeButton.style.width = customButton.clientWidth + 'px';

    // set iframe of chat
    var divCustomeChat =  document.body.querySelector('.fb-customerchat');
    var iframeCustomeChat =  divCustomeChat.querySelector('iframe');
    if (isMobile() && window.innerWidth <= 360) {
        iframeCustomeChat.style.right = 0;
        return;
    }
    else if (isMobile() && window.innerWidth <= 480) {
        return;
    }
    if (settings.buttonPosition > 50)
        iframeCustomeChat.style.left = (offsetLeft - (iframeCustomeChat.clientWidth - clientWidth)) + 'px';
    else
        iframeCustomeChat.style.left = offsetLeft + 'px';
}

window.addEventListener('resize', function() {
    if (showFB) {
        fb.refresh();
        refreshFrameFacebook();
    }
});

var Chatative = function() {
};

Chatative.prototype.createChat = function(settings) {
    var loggedInGreeting = settings.loggedInGreeting;
    var loggedOutGreeting = settings.loggedOutGreeting;
    var bubbleColor = settings.bubbleColor;
    var useDefault = settings.useDefault;

    this.div = document.createElement('div');
    this.div.setAttribute('page_id', pageId);
    this.div.setAttribute('class', 'fb-customerchat');
    if (!useDefault) {
        this.div.setAttribute('logged_in_greeting', loggedInGreeting);
        this.div.setAttribute('logged_out_greeting', loggedOutGreeting);
        this.div.setAttribute('theme_color', bubbleColor);
    }
    document.body.appendChild(this.div);
}

Chatative.prototype.createButton = function(settings) {
    this.button = document.createElement('div');
    this.button.setAttribute('class', 'btn-fb-custom');

    this.button.style.backgroundColor =  settings.buttonColor;
    this.button.style.color = settings.iconTextColor;

    var icon = document.createElement('span');
    icon.setAttribute('class', 'icon-custom ' + settings.iconOnButton);
    var text = document.createElement('span');
    text.setAttribute('class', 'icon-custom-text');
    text.textContent = settings.textOnButton;
    this.button.appendChild(icon);
    if (text && (!isMobile() || window.innerWidth > 480))
        this.button.appendChild(text);

    document.body.appendChild(this.button);
};

Chatative.prototype.refresh = function() {
    this.refreshCustomButton();
};

Chatative.prototype.refreshCustomButton = function() {
    var buttonWidth = this.button.clientWidth;
    var screenWidth = window.document.body.clientWidth;
    var left = settings.buttonPosition * (screenWidth - 48 - buttonWidth) / 100 + 24;
    this.button.style.left = left + 'px';
};


Chatative.prototype.appendChat = function() {
    this.appendStyle();
    this.createChat(settings);
}

Chatative.prototype.appendStyle = function() {
    var newStyle = document.createElement('style');
    newStyle.appendChild(document.createTextNode("\
  @font-face {\
      font-family: qfc;\
      src: url(" + urlFont + ");\
  }\
  "+ styleButton + ""));
    document.head.appendChild(newStyle);
}

Chatative.prototype.appendFB = function() {
    // this.script = document.createElement('script');
    // this.script.textContent = fbInit;
    // document.body.appendChild(this.script);
    window.fbAsyncInit = function() {
        FB.init({
            autoLogAppEvents : true,
            xfbml            : true,
            version          : 'v3.0'
        });

        FB.Event.subscribe('xfbml.render', function() {
            console.log('render done')
            var buttonDialog =  document.body.querySelector('.fb_dialog.fb_dialog_advanced');
            if (isMobile()) buttonDialog =  document.body.querySelector('.fb_dialog.fb_dialog_mobile');
            if (buttonDialog) {
                fb.createButton(settings);
                fb.refresh();
                refreshFrameFacebook();
            }
        });

    };

    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "https://connect.facebook.net/" +  settings.language + "/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
}

var isMobile = function() { return /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent);}

var fb = new Chatative();
const showFB = (settings.showMobile && isMobile() && window.innerWidth <= 480)
                || (settings.showDesktop && ( !isMobile() || window.innerWidth > 480 ));
if (showFB) {
    fb.appendFB();
    fb.appendChat();
}
