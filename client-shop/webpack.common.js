 const path = require('path');
 const CleanWebpackPlugin = require('clean-webpack-plugin');
 const HtmlWebpackPlugin = require('html-webpack-plugin');

 module.exports = {
   entry: {
     index: './src/index.js'
   },
   plugins: [
         new CleanWebpackPlugin(['dist']),
         new HtmlWebpackPlugin({
             filename: 'index.html',
             template: 'src/index.html'
         })
   ],
     output: {
     filename: '[name].js',
         path: path.resolve(__dirname, 'dist')
   }
 };
