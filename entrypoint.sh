#!/bin/bash
echo "Wait for MySql connection to $MYSQL_HOST"
while :
do
	mysqladmin ping -h $MYSQL_HOST --connect_timeout 3 --user=$MYSQL_USER --password=$MYSQL_PASSWORD 2>/dev/null 1>/dev/null
	MYSQLD_RUNNING=${?}
	if [ ${MYSQLD_RUNNING} -eq 0 ]; then break ; fi
	printf ".";
	sleep 1;
done
export DATABASE_URL=mysql://${MYSQL_USER}:${MYSQL_PASSWORD}@${MYSQL_HOST}:${MYSQL_PORT}/chat?charset=utf8mb4
python /usr/local/facebookchat/manage.py migrate
python /usr/local/facebookchat/manage.py runserver 0.0.0.0:8000
